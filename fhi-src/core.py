 #!/usr/bin/python 
import os 
import numpy as np
import threading 
import Queue
import glob 
import random
#from initialization import *
import config
from replicaexchange import *
from write_file import *
from tool import *
import os.path
from gcmc import *
import glob
import time

def REGCMD():
        timeoutput=open('count_time','a+')
        localtime = time.asctime(time.localtime(time.time()))
        timeoutput.write(str(localtime)+'\n')

	for ii in range(config.num_replica):
		srank='%.2d' %ii	
		os.system('mkdir replica_'+str(srank))
		os.system('rm '+'replica_'+str(srank)+'/aims.out ')
                os.system('rm '+'replica_'+str(srank)+'/aims_MD_restart.dat')
		os.system('cp control.in.E replica_'+srank+'/control.in')
	TGathered  = np.zeros(config.num_replica)
	uGathered  = np.zeros(config.num_replica)
	vscaling   = np.zeros(config.num_replica)
	eGathered  = np.array(config.e_list)
	pseudo_H    = np.array(config.Ham_list)
	NGathered  = np.array(config.number_particle)
	Acc_r      = np.array(config.Nacc_r)
	Acc_a      = np.array(config.Nacc_a)
	list_coord = config.coord_list
	list_veloc = config.veloc_list
	table      = np.copy(config.sequence).reshape((config.num_T,config.num_u))
	current_steplocal = config.current_step
	for i in range(config.num_replica):
		TGathered[i]  = config.T[config.index_table[i][0]]
		uGathered[i]  = config.Mu[config.index_table[i][1]]
	for loop in range(config.current_step, config.N):
		for ii in range(config.num_replica):
	                srank='%.2d' %ii
			os.system('cp control.in.E replica_'+srank+'/control.in')
		rand = random.random()
#**********************************************************************************************************************
                                      # Do a replica exchange 10%
#**********************************************************************************************************************  
		if (rand<= 0.1):
			#for i in range(config.num_replica):
	                #        vscaling[i]   = 1.0
			print 'rex', loop
#			TGathered, uGathered, vscaling, table = Neighbouring_swap_replicas(TGathered, uGathered, vscaling, eGathered, NGathered, table)
			TGathered, uGathered, table = Neighbouring_swap_replicas(TGathered, uGathered, eGathered, NGathered, table)
#***************************************************************************************************************
                        #rescaling the velocities for each replica after every replica exchange
#***************************************************************************************************************
			for ii in range(config.num_replica):
                        	list_veloc[ii]=remd_rescale_velocity(list_veloc[ii], vscaling[ii])

			for ii in range(config.num_replica):
 	                	write_geometry(list_coord[ii], list_veloc[ii], ii)
	                        #write_geometry(list_coord[ii], ii)
        	                write_md_control(TGathered[ii], ii, len(list_coord[ii]), loop)

#**********************************************************************************************************************
                                      # Parallel molecular dynamics
#**********************************************************************************************************************
                	os.system('sh subjobs')
	                for ii in range(config.num_replica):
        	                srank='%.2d' %ii
                	        aims_filename = os.path.join('replica_'+str(srank), 'aims.out')
	                        md_restart_file = os.path.join('replica_'+str(srank), 'aims_MD_restart.dat')
        	                read_out = read_md_results(aims_filename, md_restart_file)
                	        eGathered[ii]  = read_out['energy']
                        	pseudo_H[ii] = read_out['pseudoH']
	                        list_coord[ii] = read_out['structure']
                        	list_veloc[ii] = read_out['velocity']
#                       noo=len(glob.glob(os.path.join('replica_'+str(srank), 'aims.out_md*')))
#                       os.system('mv '+'replica_'+str(srank)+'/aims.out '+'replica_'+str(srank)+'/aims.out_' + 'md' + str(noo))
				os.system('cat '+'replica_'+str(srank)+'/aims.out  >>rex_' + str(float(TGathered[ii])) + '_' + str(float(uGathered[ii])))
	                        os.system('cat '+'replica_'+str(srank)+'/aims.out  >>' + str(float(TGathered[ii])) + '_' + str(float(uGathered[ii])) + '.out')
	                        os.system('cat '+'replica_'+str(srank)+'/aims.out >>'+'replica_'+str(srank)+'/trajectory')
				os.system('rm '+'replica_'+str(srank)+'/aims.out ')
                        #os.system('rm '+'replica_'+str(srank)+'/aims.out ')
#                       noo=len(glob.glob(os.path.join('replica_'+str(srank), 'aims_MD_restart_file*')))
 #                       os.system('mv '+'replica_'+str(srank)+'/aims_MD_restart.dat '+'replica_'+str(srank)+'/aims_MD_restart_file' + str(noo))


#                        os.system('rm '+'replica_'+str(srank)+'/aims_MD_restart.dat ')

        #                print list_coord[ii]
         #               print 'parallelMD', loop
#               tmp_test.write('coord'+str(list_coord)+'\n')
 #               tmp_test.write('len_coord'+str(len(list_coord))+'\n')
  #              tmp_test.write('NGathered'+str(len(NGathered))+'\n')

          	        write_data(loop, TGathered, uGathered, eGathered, pseudo_H, table, list_coord, list_veloc, Acc_a, Acc_r)
                	localtime = time.asctime(time.localtime(time.time()))
	                timeoutput.write(str(localtime)+'\n')



#**********************************************************************************************************************
                                      # Do the grand-canonical Monte Carlo(90%)
#**********************************************************************************************************************
		else:
			e_former =  eGathered
			coord_gc = np.copy(list_coord)
			veloc_gc = np.copy(list_veloc)
			mcexch_coord = []
			mcexch_veloc = []
			for ii in range(config.num_replica):
				coord_tmp, veloc_tmp=mcexch(coord_gc[ii], veloc_gc[ii], TGathered[ii])
#				coord_tmp = mcexch(coord_gc[ii])
				mcexch_coord.append(coord_tmp)
				mcexch_veloc.append(veloc_tmp)
				write_geometry(mcexch_coord[ii], mcexch_veloc[ii], ii)
#				write_geometry(mcexch_coord[ii], ii)
			os.system('sh subjobs')
			e_latter = []
			for ii in range(config.num_replica):
				srank='%.2d' %ii
                                aims_filename = os.path.join('replica_'+str(srank), 'aims.out')
				read_out      = read_scf_results(aims_filename)
                                e_latter.append(read_out['energy'])
			#	noo=len(glob.glob(os.path.join('replica_'+str(srank), 'aims.out_e*')))
                         #       os.system('mv '+'replica_'+str(srank)+'/aims.out '+'replica_'+str(srank)+'/aims.out_' + 'e' + str(noo))
                                os.system('rm '+'replica_'+str(srank)+'/aims.out ')


			for ii in range(config.num_replica):
              			if (len(list_coord[ii]) < len(mcexch_coord[ii])):
                	        	if(len(mcexch_coord[ii])-len(list_coord[ii])==1):
                	                	P_acc = accept_a(TGathered[ii], uGathered[ii], e_former[ii], e_latter[ii], NGathered[ii])
                	                	if P_acc >random.random():
                	                        	list_coord[ii]    = mcexch_coord[ii]
							list_veloc[ii]    = mcexch_veloc[ii]
        	        	                        Acc_a[ii]    	  += 1
                	        	if(len(mcexch_coord[ii])-len(list_coord[ii])==2):
		                	        P_acc = accept_a2(TGathered[ii], uGathered[ii], e_former[ii], e_latter[ii], NGathered[ii])
                		                if P_acc >random.random():
							list_coord[ii]    = mcexch_coord[ii]
                                                        list_veloc[ii]    = mcexch_veloc[ii]
                	                        	Acc_a[ii]	  += 2

  	             		elif (len(list_coord[ii]) > len(mcexch_coord[ii])):
                	        	if(len(list_coord[ii])-len(mcexch_coord[ii])==1):
                	                	P_acc = accept_r(TGathered[ii], uGathered[ii], e_former[ii], e_latter[ii], NGathered[ii])
                	                	if P_acc >random.random():
							list_coord[ii]    = mcexch_coord[ii]
                                                        list_veloc[ii]    = mcexch_veloc[ii]
        	        	                        Acc_r[ii]	  += 1
                	        	if(len(list_coord[ii])-len(mcexch_coord[ii])==2):
                	                	P_acc = accept_r2(TGathered[ii], uGathered[ii], e_former[ii], e_latter[ii], NGathered[ii])
               	                 		if P_acc >random.random():
							list_coord[ii]    = mcexch_coord[ii]
                                                        list_veloc[ii]    = mcexch_veloc[ii]
                	                        	Acc_r[ii]	  += 2



				NGathered[ii]=len(list_coord[ii])

			for ii in range(config.num_replica):
				write_geometry(list_coord[ii], list_veloc[ii], ii)
	#			write_geometry(list_coord[ii], ii)
				write_md_control(TGathered[ii], ii, len(list_coord[ii]), loop)

#**********************************************************************************************************************
                                      # Parallel molecular dynamics
#**********************************************************************************************************************
			os.system('sh subjobs')
			for ii in range(config.num_replica):
				srank='%.2d' %ii	
				aims_filename = os.path.join('replica_'+str(srank), 'aims.out')
				md_restart_file = os.path.join('replica_'+str(srank), 'aims_MD_restart.dat')
                	        read_out = read_md_results(aims_filename, md_restart_file)
				eGathered[ii]  = read_out['energy']
				pseudo_H[ii] = read_out['pseudoH']
				list_coord[ii] = read_out['structure']
				list_veloc[ii] = read_out['velocity']
#			noo=len(glob.glob(os.path.join('replica_'+str(srank), 'aims.out_md*')))
#			os.system('mv '+'replica_'+str(srank)+'/aims.out '+'replica_'+str(srank)+'/aims.out_' + 'md' + str(noo))

				os.system('cat '+'replica_'+str(srank)+'/aims.out  >>' + str(float(TGathered[ii])) + '_' + str(float(uGathered[ii])) + '.out')
				os.system('cat '+'replica_'+str(srank)+'/aims.out >>'+'replica_'+str(srank)+'/trajectory')

				os.system('rm '+'replica_'+str(srank)+'/aims.out ')
#			noo=len(glob.glob(os.path.join('replica_'+str(srank), 'aims_MD_restart_file*')))
 #                       os.system('mv '+'replica_'+str(srank)+'/aims_MD_restart.dat '+'replica_'+str(srank)+'/aims_MD_restart_file' + str(noo))

			
#                        os.system('rm '+'replica_'+str(srank)+'/aims_MD_restart.dat ')

		#	print list_coord[ii]
		#	print 'parallelMD', loop
#		tmp_test.write('coord'+str(list_coord)+'\n')
 #               tmp_test.write('len_coord'+str(len(list_coord))+'\n')
  #              tmp_test.write('NGathered'+str(len(NGathered))+'\n')

			write_data(loop, TGathered, uGathered, eGathered, pseudo_H, table, list_coord, list_veloc, Acc_a, Acc_r)
			localtime = time.asctime(time.localtime(time.time()))
		        timeoutput.write(str(localtime)+'\n')

		print 'restart', loop
			


if __name__ == '__main__': 
    REGCMD() 
    print 'REGCMD FINISHED'
