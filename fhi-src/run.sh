#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./tjob.out.%j
#SBATCH -e ./tjob.err.%j
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J REGC 
# Queue (Partition):
#SBATCH --partition=general
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=Definenumberofnodes
#SBATCH --ntasks-per-node=definenumberoftaskpernode
#
#SBATCH --mail-type=fail,end
#SBATCH --mail-user=user@emaiaddrass
#
# Wall clock limit:
#SBATCH --time=24:00:00

# Run the program:


python core.py>log.core
